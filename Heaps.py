def l(i): return 2*i + 1
def r(i): return 2*i + 2
def p(i): return (i-1)//2

class MinHeap():

    def __init__(self, data=[]):
        self.array = data
        self.size = len(data)

    def insert(self, k):
        self.array.append(k)
        self.size += 1

        self.bubble_up(len(self.array)-1)

    def bubble_up(self, i):
        while i > 0:
            if self.array[i] < self.array[p(i)]:
                self.array[i], self.array[p(i)] = self.array[p(i)], self.array[i]
                i = p(i)
            else:
                break

    def bubble_down(self, i):
        while not (l(i) >= self.size or self.array[l(i)] >= self.array[i] and r(i) >= self.size or self.array[r(i)] >= self.array[i]):
            if r(i) >= self.size or self.array[l(i)] < self.array[r(i)]:
                self.array[i], self.array[l(i)] = self.array[l(i)], self.array[i]
                i = l(i)
            else:
                self.array[i], self.array[r(i)] = self.array[r(i)], self.array[i]
                i = r(i)

    def extract_min(self):
        mini = self.array[0]
        self.array[0], self.array[-1] = self.array[-1], self.array[0]
        del self.array[-1]
        self.bubble_down(0)
        return mini

class SpecialMinHeap(MinHeap):

    def decrease_key(self, i, k):
        assert self.array[i] >= k
        self.array[i] = k
        self.bubble_up(i)

    def delete(self, i):
        assert i < self.size
        self.array[i], self.array[-1] = self.array[-1], self.array[i]
        del self.array[-1]
        if self.array[i] < self.array[p(i)]:
            self.bubble_up(i)
        else:
            self.bubble_down(i)
